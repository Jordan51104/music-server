const express = require('express');
const Handlebars = require('handlebars');
const exphbs = require('express-handlebars');
const Grid = require('gridfs-stream');
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access')

require('dotenv').config()

const app = express();
const port = process.env.PORT || 3000;

//app.engine('handlebars', exphbs({ defaultLayout: 'main', handlebars: allowInsecurePrototypeAccess(Handlebars) }));
//app.set('view engine', 'handlebars');
app.set('view engine', 'pug')

//////////////////////////////////////////////////
// setup mongoose

const { db, mongoose, conn, Song, Album } = require('./db/db');

let gfs;

db.once('open', function() {
	gfs = Grid(db.db, mongoose.mongo);
});
///////////////////////////////////////////////////

app.get('/', (req, res) => {
	res.render('index', { title: "you", message: "gay"});
})

app.use('/app', require('./routes/app/app'));
app.use('/api', require('./routes/api/api'));

app.use(express.static('./public'));


app.listen(port, () => {
	console.log(`listening on ${port}`);
});
