const express = require('express');

const router = express.Router();
const { Album } = require('../../db/db')

router.get('/', (req, res) => {
	res.redirect('/app/view/album')
});

router.use('/create', require('./create.js'))
router.use('/view', require('./view.js'))

module.exports = router;